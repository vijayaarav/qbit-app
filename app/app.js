import Vue from 'nativescript-vue';

// import Dash from './components/Dashboard';

import routes from "./routes";
import BackendService from "./services/backend-service";
import { TNSFontIcon, fonticon } from './nativescript-fonticon';
// Uncommment the following to see NativeScript-Vue output logs
// Vue.config.silent = false;

// new Vue({

//   render: h => h('frame', [h(Dash)])

// }).$start();



// Uncommment the following to see NativeScript-Vue output logs
// Vue.config.silent = false;

const backendService = new BackendService();
Vue.prototype.$backendService = backendService;

TNSFontIcon.debug = false;
TNSFontIcon.paths = {
    'fa': './fonts/font-awesome.css',
    'ion': './fonts/ionicons.css',
};
TNSFontIcon.loadCss();
Vue.filter('fonticon', fonticon);
new Vue({
  render: h => h("frame", [h(backendService.isLoggedIn() ? routes.home : routes.login)])
}).$start();
